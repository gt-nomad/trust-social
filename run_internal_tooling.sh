#!/bin/bash
docker stop tcpserver
docker stop tcpclient
docker stop syscall
docker rm tcpserver
docker rm tcpclient
docker rm syscall
sudo rm /var/lib/docker/volumes/datavolume/_data/.testsock_server
docker run -it -d --name tcpserver -v datavolume:/tmp/volume trust/tcpserver /bin/bash
docker run -it -d --name tcpclient -v datavolume:/tmp/volume trust/tcpclient /bin/bash
docker run -it -d --name syscall -v datavolume:/tmp/volume trust/syscall /bin/bash
