package main

import (
    "encoding/json"
    "fmt"
    "net"
)

type sockOut struct {
    Age   uint32 `json:"age"`
    Id    uint32 `json:"id"`
    Name  string `json:"Name"`
}

type sockIn struct {
    Name   string `json:"Name"`
    Age    string `json:"age"`
}

func main() {
    c, err := net.Dial("unix", "/tmp/volume/.testsock_server")
    if err != nil {
        panic(err)
    }
    defer c.Close()

    decode := json.NewDecoder(c)
    encode := json.NewEncoder(c)

    inputJSON := sockIn{
       Name: "",
       Age: "",
    }
    err = encode.Encode(inputJSON)

    var outputJSON sockOut
    err = decode.Decode(&outputJSON)

    if err != nil {
        fmt.Printf("Name is %v", err)
    }
    fmt.Printf("Age: %d, Id: %d Name: %s\n", outputJSON.Age, outputJSON.Id, outputJSON.Name)
}
