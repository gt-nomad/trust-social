package main

import (
    "encoding/json"
    "fmt"
    "net"
    "os"
)

var sockLocArg = "/tmp/volume/.testsock_server"

type sockOut struct {
    Age  uint32 `json:"Age"`
    Id   uint32 `json:"Id"`
    Name string `json:"Name"`
}

type sockIn struct {
    Name   string `json:"Name"`
}

func processSockRequest(c net.Conn) {

    defer c.Close()
    decode := json.NewDecoder(c)
    encode := json.NewEncoder(c)

    var inputJSON sockIn
    err := decode.Decode(&inputJSON)
    if err != nil {
        fmt.Printf("Error is %v", err)
    }
    fmt.Printf("Name: %s\n", inputJSON.Name)

    outputJSON := sockOut{
        Age:  25,
        Id:   1,
        Name: "John Doe",
    }
    err = encode.Encode(outputJSON)
    if err != nil {
        fmt.Printf("Error is %v", err)
    }
}

func initSocket() {
    localSocket, err := net.Listen("unix", sockLocArg)
    if err != nil {
        fmt.Printf("Unable to create unix domain socket. Error: %v", err)
        os.Exit(1)
    }
    if err = os.Chmod(sockLocArg, 0700); err != nil {
        fmt.Printf("Unable to change the permissions for the socket. Error: %v", err)
        os.Exit(1)
    }
    for {
        SockFileDescriptor, err := localSocket.Accept()
        if err != nil {
            fmt.Printf("Unable to accept incoming messAges over the socket. Error: %v", err)
            os.Exit(1)
        }
        processSockRequest(SockFileDescriptor)
    }
}

func main() {
    initSocket()
}
