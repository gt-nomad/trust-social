#!/bin/bash
docker volume create -- name datavolume
docker build -t trust/tcpserver . -f Dockerfile_tcpserver
docker build -t trust/tcpclient . -f Dockerfile_tcpclient
docker build -t trust/syscall . -f Dockerfile_syscall
